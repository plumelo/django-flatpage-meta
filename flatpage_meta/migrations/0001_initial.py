# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FlatPageMetaTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=150)),
                ('flatpage', models.ForeignKey(related_name='meta_tag_set', to='flatpages.FlatPage')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Meta tag',
            },
        ),
        migrations.CreateModel(
            name='MetaTagType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=30)),
                ('format_string', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=150, null=True, blank=True)),
                ('allow_multiple', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='SiteMetaTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=150)),
                ('meta_tag_type', models.ForeignKey(related_name='flatpage_meta_sitemetatag_related', to='flatpage_meta.MetaTagType')),
                ('site', models.ForeignKey(related_name='meta_tag_set', to='sites.Site')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Meta tag',
            },
        ),
        migrations.AddField(
            model_name='flatpagemetatag',
            name='meta_tag_type',
            field=models.ForeignKey(related_name='flatpage_meta_flatpagemetatag_related', to='flatpage_meta.MetaTagType'),
        ),
    ]
